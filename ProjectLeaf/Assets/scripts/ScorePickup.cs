﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePickup : MonoBehaviour
{
    // Public variable
    //editable in unity
    //usable by other scripts
    public int pickupValue = 1;

    // called in unity when this object overlaps with another object marked as a trigger
    //this is our condition ("if the player touches a coin")
    private void OnTriggerEnter2D(Collider2D other)
    {
        //check if score script is attached to the thing we bumped into
        Score scoreScript = other.GetComponent<Score>();

        if (scoreScript !=null)
        {
            //we have a scoreScript, so the thing we bumped into is the player!

            //add our pickup value to the player's current score (actions)
            scoreScript.AddScore(pickupValue);

            //we should then delete this object so we don't infinitely add score
            Destroy(gameObject);
        }

    }
}
