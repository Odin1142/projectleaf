﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    //This will be the amount of damage the hazard does
    //public means editiable in unity
    //int = whole number
    public int HealthRestore;


    //built in unity function for handling collision
    //This fucntion will be called when another object bumps
    //into the one this script is attached to
    private void OnCollisionEnter2D(Collision2D collisionData)
    {
        //get the object we collided with
        Collider2D ObjectWeCollidedWith = collisionData.collider;

        //Get player health script attached to that object (if there is one)
        PlayerHealth player = ObjectWeCollidedWith.GetComponent<PlayerHealth>();

        //check if we actually found a player health script on the object we collided with
        //This if statement is true if the player varaiable is Not null (aka empty)
        if (player != null)
        {
            //This means there WAS a PlayerHealth script attached to the object we bumped into
            //Which means this object is indeed the player

            //Therefore perform our action
            player.ChangeHealth(HealthRestore);

        }
    }

}
