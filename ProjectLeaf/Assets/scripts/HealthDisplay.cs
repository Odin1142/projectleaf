﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDisplay : MonoBehaviour
{

    //This will contain a list of the game objects for the icons
    //public - shown in unity
    //[] - this will be an array (list)
    //gameObject[] - this list will contain gameObjects
    public GameObject[] healthIcons;

    //this will contain the player health component that is on the player game object
    //so we can ask it for info about the player's health
    PlayerHealth player;
    
    // Start is called before the first frame update
    void Start()
    {
        //we search the scene for the object with PlayerHealth attached
        //store the PlayerHealth component from that object in our player variable
        player = FindObjectOfType<PlayerHealth>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //create a variable to keep track of which item in the list we are on
        //and how much health that icon is worth
        int iconHealth = 0;

        //go through each icon in the list
        //we will do everything inside the brackets for each item in list
        //for each step in the loop, we'll store the current item list in the "icon" variable
        foreach (GameObject icon in healthIcons)
        {
            //each icon is worth 1 more health than the last
            //get the current health, add 1 to it.
            //and store the result back into the iconHealth variable
            iconHealth = iconHealth + 1;
            
            //if the player's current health is equal or greater
            //than the health value for this icon....
            if (player.GetHealth() >= iconHealth)
            {
                //then turn this ON
                icon.SetActive(true);
            }
            //oterwise
            //the player's health is LESS than this icon's value
            else
            {
                //turn the icon OFF
                icon.SetActive(false);
            }
        }
    }
}
