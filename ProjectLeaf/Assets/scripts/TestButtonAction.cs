﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButtonAction : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TestButton()
    {
        //any code inside this function will run
        //when the button is clicked
        Debug.Log("Button has been clicked!");
    }
}
