﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    //public variable
    public Text scoreDisplay;
    public bool shouldReset = false;
    public int winningScore;
    public string winningScene; 
    //private variables
    private static int scoreValue = 0;

    //called by uniity the frame after this object is created
    private void Start()
    {
        //check if we should be resetting the score this scene
        if (shouldReset == true)
        {
            //reset the score value back to 0
            scoreValue = 0;
        }

        //update the display of the score based on the numerical value
        scoreDisplay.text = scoreValue.ToString();
    }

    //function to add to the player's score
    // NOT automatically called to unity - we need to call it ourselves in our code.
    public void AddScore(int toAdd)
    {
        // update the numerical value of the score (action 1)
        scoreValue = scoreValue + toAdd;

        //update the display of the score based on the numerical value
        scoreDisplay.text = scoreValue.ToString();

        //check if score is bigger than win score
        if(scoreValue >= winningScore)
        {
            //our score si a winning score! load win scene!
            SceneManager.LoadScene(winningScene);
        }
    }
}
