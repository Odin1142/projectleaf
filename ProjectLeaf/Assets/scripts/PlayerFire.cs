﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    // Unity editor variable
    public GameObject ProjectilePrefab;
    public Vector2 projectileVelocity;


    // ACTION: Fire a projectile
    public void FireProjectile()
    {
        // clone the projectile
        //declare a variable to holf the cloned object
        GameObject clonedProjectile;
        //use the instantiate to clone the projectile and keept the result in our variable
        clonedProjectile = Instantiate(ProjectilePrefab);

        //position the projectile on the player
        clonedProjectile.transform.position = transform.position; //optional : add an offest (use a public variable)

        //fire it in a direction
        //declare a variable to hold the cloned object's rigidbody
        Rigidbody2D projectileRigidbody;
        //get the rigidbody from our cloned projectile and store it
        projectileRigidbody = clonedProjectile.GetComponent<Rigidbody2D>();
        //set the velocity on the rigidbody tot he editor setting
        projectileRigidbody.velocity = projectileVelocity;


    }
}
