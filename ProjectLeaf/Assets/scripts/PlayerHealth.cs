﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    //This will be the starting health for the player
    // public = shown in the unity editor and accesible from other scripts
    //int = whole number
    public int startingHealth;
    //can be edited in unity
    public string gameOverScene;
    //this will be the player's current health and will change as teh game goes on
    int currentHealth;

    //build in unity function called when the script is created
    //usually when the game starts
    //this happens before the start() function
    private void Awake()
    {
        //initialise out current health to be equal to our
        //starting heath a th the beginning of the game
        currentHealth = startingHealth;
    }

    //NOT build into unity
    //we must call it ourselves
    //This will change the player's current health
    // and Kill() them if they have 0 health or less
    //public so other scripts can access it
    public void ChangeHealth(int ChangeAmount)
    {
        //take our current health, add the change amount and store
        //the result in the current health variable
        currentHealth = currentHealth + ChangeAmount;

        //keep our current health between 0 and the starting value
        currentHealth = Mathf.Clamp(currentHealth, 0, startingHealth);
        
        //if our health drops to 0, that means the player should die
        if (currentHealth <= 0)
        {
            //we call the kill function to kill the player
            Kill();
        }
    }

    //This function is not build into unity
    // It will only be called manually by our own code
    //It must be marked "public" sp pur other scripts can access it
    public void Kill()
    {
        //this will destroy the gameobject that this scrupt is attached to
        Destroy(gameObject);

        // load the game over scene
        SceneManager.LoadScene(gameOverScene);
    }


    public int GetHealth()
    {
        return currentHealth;
    }
}
