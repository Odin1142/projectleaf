﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{

    //condition: when the projectile hits a certain type.... (enemy)
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        //check if the object we collided with has the tag we are looking for (Enemy)
        if (otherCollider.CompareTag("Enemy") ==true)
        {
            //perform our action
            KillEnemy(otherCollider.gameObject);
        }
    }

    //action : destroy an object (enemy) 
    public void KillEnemy(GameObject enemy)
    {

        //check if the enemy ahs the "ScoreKill" component
        ScoreKill scoreKillScript = enemy.GetComponent<ScoreKill>();
        if (scoreKillScript != null)
        {
            //this mean our enemy has a score value
            Score playerScore = FindObjectOfType<Score>();

            //add the value of the scoreKillScript
            playerScore.AddScore(scoreKillScript.killValue);

        }
        //destroy the enemy
        Destroy(enemy);
    }
}
